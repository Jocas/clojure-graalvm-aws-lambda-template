# clojure-graalvm-aws-lambda-template

[![Clojars Project](https://img.shields.io/clojars/v/clojure-graalvm-aws-lambda/lein-template.svg)](https://clojars.org/clojure-graalvm-aws-lambda/lein-template)
[![pipeline status](https://gitlab.com/Jocas/clojure-graalvm-aws-lambda-template/badges/master/pipeline.svg)](https://gitlab.com/Jocas/clojure-graalvm-aws-lambda-template/commits/master)

A Leiningen template for AWS Lambda custom runtime with GraalVM compiled Clojure project.

Published in [Clojars](https://clojars.org/clojure-graalvm-aws-lambda/lein-template)

## Usage

Run:
```
lein new clojure-graalvm-aws-lambda your-lambda
```

This results in a project structure like this:
```
$ tree -a your-lambda
your-lambda
├── bootstrap
├── deps.edn
├── Dockerfile
├── .gitignore
├── .gitlab-ci.yml
├── lambda.yml
├── Makefile
├── README.md
└── src
    └── lambda
        └── core.clj

2 directories, 9 files

```

Then 
```
cd your-lambda
```

Set these environment variables:
- MY_AWS_DEFAULT_REGION
- MY_AWS_ACCESS_KEY_ID
- MY_AWS_SECRET_ACCESS_KEY
- MY_S3_BUCKET
- MY_S3_FOLDER

Run:
```
make deploy-lambda-via-container
```

Lambda is ready to be used. Go to your AWS Console to work with the new stack named `lambda-custom-runtime-your-lambda`.

## License

Copyright © 2019 Dainius Jocas

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

This Source Code may also be made available under the following Secondary
Licenses when the conditions for such availability set forth in the Eclipse
Public License, v. 2.0 are satisfied: GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or (at your
option) any later version, with the GNU Classpath Exception which is available
at https://www.gnu.org/software/classpath/license.html.
